# Linux Application
Open the Terminal and type the following command `sudo apt-get update` to update the packages.

# Node.Js Installation
In the Terminal, type the following command `sudo apt-get install nodejs` to install nodejs.

# NPM Installation
In the Terminal, type the following command `sudo apt-get install npm` to install npm.

# Git Installation
In the Terminal, type the following command `sudo apt-get install git` to install git.

# GitSearch

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.2.

# Running the project
In the Git Bash, type the following command `git clone %https://gitlab.com/project.com/yourproject/yourproject.git%` to clone the repository, after this open the folder created with the command `CD %dirname%` and type`ng serve --open`.
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

